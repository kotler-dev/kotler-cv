## Михайлов Антон Вячеславович
Telegram [@Kotlerdev](https://t.me/kotlerdev)  
https://instagram.com/kotler.dev  
https://gitlab.com/kotler-dev  
https://github.com/kotler-dev   

Город проживания: Москва, м. Лермонтовский проспект  
Гражданство: Россия   

### Желаемая должность
Java Developer  
Занятость: полная занятость  
График работы: полный день  
Желательное время в пути до работы: не имеет значения

### Ключевые навыки
- Java: Java Core, Spring, Spring Boot, Lombok, Maven.  
- Kotlin: Kotlin Core, Jetpack Compose.  
- Database: JDBC, SQL, PostgreSQL, H2, Hibernate, JPA.  
- Web: HTML, CSS, JS, Selenium, Figma, Miro.  
- CVS: Git CLI, Gitlab, Github, Confluence.  
- Pattern: MVC, CRUD.  
- Linux: Fedora Gnome DE, Archlinux, Debian.  
- Management: Jira, Trello.  

### АНО ВО «Университет Иннополис» 12/2021

Курс «Основы разработки ПО на Java»:  

- Основы синтаксиса и инфраструктуры Java;  
- Процедурное программирование в Java;  
- ООП в Java, инструменты для разработки;  
- Библиотека классов Java;  
- Алгоритмы и структуры данных;  
- Многопоточность;  
- PostgreSQL;  
- H2;  
- GIT;  
- IntelliJ IDEA;  
- Модульное тестирование, JUnit.

Разработал CRUD клиент-серверное приложение с веб интерфейсом и сохранением данных в БД Postgres.    
Ссылка на проект "Учёт данных о пользователе":
https://gitlab.com/kotler-dev/innopolis-spring-boot  

Стек: Java, Spring, Spring Boot, Lombok, JDBC, SQL, PostgreSQL, Hibernate, JPA, MVC, CRUD, HTML, Maven.

### Дипломный проект
Разработка дипломного проекта: "Квиз по изучению Английского языка"  
Структура БД https://docs.google.com/spreadsheets/d/1l-xP03LF-S66cOy_9-f7U4W5H6Uv0kD5d87K3QhD6bo/edit?usp=sharing  

### Проектная работа

- Адаптивная вёрстка макетов.  
Пример https://kotler-dev.github.io  
Стек: HTML5, CSS3.  
- Разработка чат-бота на Python для курсов по SEO.  
Стек: Python, Heroku, Postgres.  
- Разработка парсера на JavaScript с Selenium для сбора и обработки данных из поисковой выдачи тематических сайтов.  
Стек: JS, Selenium, NodeJS, CSV, JSON.  
- Разработка простого Квиза на Kotlin под Android используя несколько Activity и Data class.  
Пример https://github.com/kotler-dev/lint-flow-quiz  
Стек: Kotlin, Jetpack Compose.  

### Читаю
- Кей Хорстман, Java - Том 1. Основы.
- Кей Хорстман, Java - Том 2. Расширенные средства программирования.
- Брюс Эккель, Философия Java.
- Адитья Бхаргава - Грокаем алгоритмы.
- Android - Программирование для профессионалов.

### Дополнительная информация
Осваиваю курс по Java на Javarush.  
Помогаю начинающим с поиском ответов на Тостер, Кью.  
Имею опыт в преподавании Информатики школьникам 5-9 классов, основ программирования, основ электроники (электротехники).  
В начале карьеры работал в службе поддержки хостинга ValueHost, Megagroup.  
Заинтересован в развитии IT сферы и считаю, что благодаря моему опыту смогу быть максимально полезным и эффективным разработчиком.  

### Хобби
- Собрал часы на газоразрядных индикаторах ГРИ-14, спаял плату, запрограммировал на Си, управление с Android черезез Bluetooth. Собрал светодиодный куб 16х16.  
- Страйкбол  
